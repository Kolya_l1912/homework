package Presenter;

import Model.Clothes;
import Model.type.ClothesType;
import View.ShopView;

import java.lang.reflect.Array;
import java.util.*;


public class ShopPresenter {
    private ShopView shopView;

    private List<Clothes> listClothes = new ArrayList<>();

    public ShopPresenter(ShopView shopView) {
        this.shopView = shopView;
        initData();
        sortCollectionId();
    }

    private boolean checkExistenClothesById(int id){
        for(Clothes item: listClothes){
            if(id == item.getId()){
                return true;
            }
        }
        return false;
    }

    private void initData() {
        listClothes.add(new Clothes(1, "Джогери", "M", "Navy", ClothesType.TROUSERS));
        listClothes.add(new Clothes(2, "Рюкзак", "", "Gray", ClothesType.BACKPACK));
        listClothes.add(new Clothes(3, "Куртка", "L", "Black", ClothesType.JACKET));
        listClothes.add(new Clothes(4, "Бомбер", "S", "Green", ClothesType.JACKET));
        listClothes.add(new Clothes(5, "Джинси", "XL", "Blue", ClothesType.TROUSERS));
        listClothes.add(new Clothes(6, "Шорти", "XS", "Red", ClothesType.SHIRT));
        listClothes.add(new Clothes(7, "Взуття", "41", "White", ClothesType.SHOES));

    }

    public void startShopWork() {

        do {
            shopView.variationOfWork();
        }while(true);
    }

    public void showAllProducts() {
        for (Clothes item : listClothes) {
            shopView.showProduct(item);
        }
    }

    public void deleteById(int id) {
        Clothes deletedClothes = null;
        for (Clothes item : listClothes) {
            if (id == item.getId()) {
                deletedClothes = item;
            }
        }
        if (deletedClothes == null) {
            shopView.showData("This id " +id+ " not found");
            return;
        }
        listClothes.remove(deletedClothes);
    }

    public void endWorkShop() {
        System.exit(0);
    }

    public void createClothes() {
        Clothes newClothes = shopView.createProduct();
        if(checkExistenClothesById(newClothes.getId())){
            shopView.showData("This id exist");
            return;
        }

        listClothes.add(newClothes);

    }
   public void sortCollectionId(){
        Collections.sort(listClothes,Comparator.comparing(Clothes::getId));
//        listClothes.sort(Comparator.comparing(Clothes::getId));
    }


}
