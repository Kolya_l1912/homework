package Model;

import Model.type.ClothesType;

public class Clothes {
    private int id;
    private String nameClothes;
    private String size;
    private String colorClothes;
    private ClothesType clothesType;

    public Clothes() {
    }

    public Clothes(int id, String nameClothes, String size, String colorClothes, ClothesType clothesType) {
        this.id = id;
        this.nameClothes = nameClothes;
        this.size = size;
        this.colorClothes = colorClothes;
        this.clothesType = clothesType;
    }

    public String getColorClothes() {
        return colorClothes;
    }

    public void setColorClothes(String colorClothes) {
        this.colorClothes = colorClothes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameClothes() {
        return nameClothes;
    }

    public void setNameClothes(String nameClothes) {
        this.nameClothes = nameClothes;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public ClothesType getClothesType() {
        return clothesType;
    }

    public void setClothesType(ClothesType clothesType) {
        this.clothesType = clothesType;
    }
}
