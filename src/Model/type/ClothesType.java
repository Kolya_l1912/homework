package Model.type;

public enum ClothesType {
    SHOES,
    JACKET,
    SHIRT,
    BACKPACK,
    TROUSERS
}
