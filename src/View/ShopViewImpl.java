package View;

import Model.Clothes;
import Model.type.ClothesType;
import Presenter.ShopPresenter;

import java.util.Scanner;

public class ShopViewImpl implements ShopView {

    private ShopPresenter mPresenter;

    public ShopViewImpl() {
        mPresenter = new ShopPresenter(this);
        mPresenter.startShopWork();
    }
    private ClothesType applyClothesType(int index){
        switch (index){
            case 1:
                return ClothesType.SHOES;
            case 2:
                return ClothesType.JACKET;
            case 3:
                return ClothesType.SHIRT;
            case 4:
                return ClothesType.BACKPACK;
            case 5:
                return ClothesType.TROUSERS;

                default:return null;
        }
    }

    private String getDataFromConsole() {
        return new Scanner(System.in).next();
    }

    @Override
    public Clothes createProduct() {
        showData("Enter clothes id");
        int clothesId = Integer.parseInt(getDataFromConsole());
        showData("Enter name clothes");
        String nameCloth = getDataFromConsole();
        showData("Enter size");
        String size = getDataFromConsole();
        showData("Enter color clothes");
        String colorCloth = getDataFromConsole();
        showData("Enter type clothes: 1-Shoes, 2-Jacket, 3-Shirt, 4-Backpack, 5-Trousers");

        ClothesType exampleClothesType;
        boolean isClothesTypeNotValid = true;
        do{
           exampleClothesType = applyClothesType(Integer.parseInt(getDataFromConsole()));
           if(exampleClothesType != null){
               isClothesTypeNotValid = false;
           }else{
               System.out.println("You enter wrong type");
           }
        }while(isClothesTypeNotValid);
        return new Clothes(clothesId, nameCloth, size, colorCloth,exampleClothesType);
    }

    @Override
    public void showData(String message) {
        System.out.println(message);
    }

    @Override
    public void showStartShopWork() {

    }

    @Override
    public void showProduct(Clothes clothes) {
        System.out.println("id ->" + clothes.getId() + "\n" +
                "name ->" + clothes.getNameClothes() + "\n" +
                "size ->" + clothes.getSize() + "\n" +
                "color ->" + clothes.getColorClothes() + "\n" +
                "type ->" + clothes.getClothesType() + "\n" +
                "==========================================");
    }

    @Override
    public void variationOfWork() {
        System.out.println("1 - Search clothes id");
        System.out.println("2 - Show all clothes");
        System.out.println("3 - Delete cloth by id");
        System.out.println("4 - Create my clothes");
        System.out.println("5 - end work shop");
        Scanner scanner = new Scanner(System.in);
        switch (scanner.nextInt()) {
            case 1:

                break;
            case 2:
                mPresenter.showAllProducts();
                break;
            case 3:
                System.out.println("Enter clothes id");
                mPresenter.deleteById(Integer.parseInt(getDataFromConsole()));
                break;
            case 4:
                mPresenter.createClothes();
                break;
            case 5:
                mPresenter.endWorkShop();
                break;
        }
    }
}
