package View;

import Model.Clothes;

public interface ShopView {
    Clothes createProduct();
    void showData(String message);
    void showStartShopWork();
    void showProduct(Clothes clothes);
    void variationOfWork();
}
